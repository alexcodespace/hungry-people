<?php
/**
 * Template Name: Home page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<?php if ( is_front_page() ) : ?>
	<?php get_template_part( 'global-templates/hero' ); ?>
<?php endif; ?>


	<div class="wrapper" id="full-width-page-wrapper">
		<section class="hp-head" id="head" <?php $image = get_field('background_head');
		if( !empty( $image ) ): ?>
			style="background:  linear-gradient(0deg, rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.6)),  url('<?php the_field('background_head') ?>');
					background-size: cover;
					background-attachment: scroll;
					background-position: center;"
		<?php endif; ?>>
			<div class="container-fluid">
				<div class="head-block">
					<div class="hp-time-work">
						<?php
						if( have_rows('time_to_work', 'option') ): ?>
							<p>
								<?php while ( have_rows('time_to_work', 'option') ) : the_row(); ?>
									<?php the_sub_field('days');?> : <?php the_sub_field('time'); ?>
								<?php endwhile; ?>
							</p>
						<?php endif; ?>
					</div>
					<div class="hp-head-title">
						<?php if(get_field('pre_title')):?>
							<h5><?php the_field('pre_title'); ?></h5>
						<?php endif;?>
						<?php if(get_field('title')):?>
							<h1><?php the_field('title'); ?></h1>
						<?php endif;?>
						<?php
						if( have_rows('buttons') ): ?>
							<div class="head-button">
								<?php while ( have_rows('buttons') ) : the_row(); ?>
									<a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('title');?></a>
								<?php endwhile; ?>
							</div>
						<?php endif; ?>
					</div>
					<div class="hp-socials">
						<?php if(get_field('facebook', 'option')):?>
							<a  class="social-item" href="<?php the_field('facebook', 'option'); ?>" target="_blank"><i class="fa fa-facebook" target="_blank"></i></a>
						<?php endif;?>
						<?php if(get_field('twitter', 'option')):?>
							<a class="social-item" href="<?php the_field('twitter', 'option'); ?>" target="_blank"><i class="fa fa-twitter" target="_blank"></i></a>
						<?php endif;?>
						<?php if(get_field('instagram', 'option')):?>
							<a class="social-item" href="<?php the_field('instagram', 'option'); ?>" target="_blank"><i class="fa fa-instagram" target="_blank"></i></a>
						<?php endif;?>
					</div>
					<div class="arrow-down">
						<a href="#hp-about">
							<i class="fa fa-angle-down" aria-hidden="true"></i>
						</a>
					</div>
				</div>
			</div>
		</section>

		<section class="hp-about" id="hp-about">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-md-6 col-12 text-center">
						<?php if(get_field('title_about')):?>
							<h2><?php the_field('title_about'); ?></h2>
						<?php endif;?>
						<?php if(get_field('caption_about')):?>
							<p><strong><?php the_field('caption_about'); ?></strong></p>
						<?php endif;?>
						<?php if(get_field('content_about')):?>
							<p><?php the_field('content_about'); ?></p>
						<?php endif;?>
					</div>
					<div class="col-md-6 col-12">
						<?php
						$imageAbout = get_field('image_about');
						$sizeAbout = 'about-block';
						$thumb = $imageAbout['sizes'][ $sizeAbout ];
						if(!empty($imageAbout)): ?>
							<img class="img-block" src="<?php echo esc_url($thumb); ?>" alt="">
							<div class="hp-square-right"></div>
						<?php endif;?>
					</div>
				</div>
			</div>
		</section>

		<section class="hp-team" id="hp-team" <?php $image = get_field('background_section_team');
		if( !empty( $image ) ): ?>
			style="background:  linear-gradient(0deg, rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.6)), url('<?php the_field('background_section_team') ?>');
				background-size: cover;
				background-attachment: scroll;
				background-position: center;"
		<?php endif; ?>>
			<div class="container">
				<div class="row align-items-center">
					<div class="col-12 text-center">
						<?php if(get_field('title_section_team')):?>
							<h5><?php the_field('title_section_team'); ?></h5>
						<?php endif;?>
					</div>
					<div class="col-md-6 col-12">
						<?php
						$imageAbout = get_field('image_team');
						$sizeAbout = 'about-block';
						$thumb = $imageAbout['sizes'][ $sizeAbout ];
						if(!empty($imageAbout)): ?>
							<img class="img-block"  src="<?php echo esc_url($thumb); ?>" alt="">
							<div class="hp-square-left"></div>
						<?php endif;?>
					</div>
					<div class="col-md-6 col-12 text-center">
						<?php if(get_field('title_team')):?>
							<h2><?php the_field('title_team'); ?></h2>
						<?php endif;?>
						<?php if(get_field('caption_team')):?>
							<p><strong><?php the_field('caption_team'); ?></strong></p>
						<?php endif;?>
						<?php if(get_field('content_team')):?>
							<p><?php the_field('content_team'); ?></p>
						<?php endif;?>
					</div>
				</div>
			</div>
		</section>

		<section class="hp-booking" id="hp-booking">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-md-6 col-12">
						<?php if(get_field('title_booking')):?>
							<h2><?php the_field('title_booking'); ?></h2>
						<?php endif;?>

						<?php //echo do_shortcode("[booking]");?>
						<?php if(get_field('form_shortcode')):?>
							<?php echo do_shortcode(get_field('form_shortcode'));?>
						<?php endif;?>
					</div>
					<div class="col-md-6 col-12">
						<?php
						$imageAbout = get_field('image_booking');
						$sizeAbout = 'about-block';
						$thumb = $imageAbout['sizes'][ $sizeAbout ];
						if(!empty($imageAbout)): ?>
							<img class="img-block" src="<?php echo esc_url($thumb); ?>" alt="">
							<div class="hp-square-right"></div>
						<?php endif;?>
					</div>
					<div class="col-12 text-center">
						<?php
						if( have_rows('time_to_work', 'option') ): ?>
							<p>
								<?php while ( have_rows('time_to_work', 'option') ) : the_row(); ?>
									<?php the_sub_field('days');?> : <strong><?php the_sub_field('time'); ?></strong>,
								<?php endwhile;
								if(get_field('phone', 'option')):?>
									Phone: <strong><?php the_field('phone', 'option'); ?></strong>
								<?endif;?>
							</p>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</section>

		<section class="hp-specialties" id="hp-specialties" <?php $image = get_field('background_section_specialties');
		if( !empty( $image ) ): ?>
			style="background:  linear-gradient(0deg, rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.6)),  url('<?php the_field('background_section_specialties'); ?>');
				background-size: cover;
				background-attachment: scroll;
				background-position: center;"
		<?php endif; ?>>
			<div class="container">
				<div class="row">
					<div class="col-12 text-center">
						<?php if(get_field('title_specialties')):?>
							<h5><?php the_field('title_specialties'); ?></h5>
						<?php endif;?>
					</div>
				</div>
			</div>
			<?php $arg = array(
				'post_type'	        => 'Specialty',
				'order'		        => 'ASC',
				'orderby'	        => 'menu_order',
				'posts_per_page'    => -1
			);
			$testimonial = new WP_Query( $arg );
			if ( $testimonial->have_posts() ) : ?>
				<div id="hp-specialties-slide" class="slick-slider" >
					<?php while ( $testimonial->have_posts() ) : $testimonial->the_post(); ?>
						<div class="slick-slide">
							<div class="slider-caption">
								<div class="container">
									<div class="row align-items-center">
										<div class="col-md-6 col-12">
											<?php the_post_thumbnail('about-block', ['class' => 'image-block']);?>
											<div class="hp-square-left"></div>
										</div>
										<div class="col-md-6 col-12 text-center">
											<h2><?php the_title();?></h2>
											<?php if( get_field('pre_description')):?>
												<p class="sg-testimonial-content"><strong><?php the_field('pre_description'); ?></strong></p>
											<?php endif;?>
											<p><?php the_content();?></p>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php endwhile; ?>
				</div><!-- END of  #home-slider-->
			<?php endif; wp_reset_query(); ?>
		</section>

		<section class="hp-menu" id="hp-menu">
			<div class="container">
				<div class="row">
					<div class="col-12 text-center">
						<?php if(get_field('title_menu')):?>
							<h2><?php the_field('title_menu'); ?></h2>
						<?php endif;?>
						<?php if(get_field('description_menu')):?>
							<p><strong><?php the_field('description_menu'); ?></strong></p>
						<?php endif;?>
					</div>
					<div class="col-12">
						<?php
						$taxonomy = 'menu_categories';
						$terms = get_terms(array(
								'taxonomy' => array($taxonomy),
								'orderby' => 'id',
								'order' => 'ASC',
						)); // Get all terms of a taxonomy

						if ( $terms && !is_wp_error( $terms ) ) :?>
							<ul class="nav nav-tabs" id="menu-tab" role="tablist">
								<?php foreach ( $terms as $indexN => $term ) {?>
									<li class="tab-items">
										<a class="nav-link <?php if ($indexN==0){ echo 'active'; }else{ echo "";}?>" href="#<?php echo $term->slug; ?>" id="<?php echo $term->slug; ?>-tab" role="tab" aria-controls="<?php echo $term->name; ?>" aria-selected="<?php if ($indexN==0){ echo 'true'; }else{ echo "false";}?>"><?php echo $term->name; ?></a>
									</li>
								<?php } ?>
							</ul>
						<?php endif;?>

						<div class="tab-content" id="tabContent">
							<?php
							$taxonomy = 'menu_categories';
							$terms = get_terms(array(
									'taxonomy' => array($taxonomy),
									'orderby' => 'id',
									'order'	=> 'ASC',
							)); // Get all terms of a taxonomy

							if ( $terms && !is_wp_error( $terms ) ) :
								foreach ($terms as $index => $term) {
									/*echo "<pre>";
									var_dump($terms);
									echo "</pre>";*/?>

									<?php $arg = array(
											'post_type'	=> 'Menus',
											'tax_query' => array(
													array(
															'taxonomy' => 'menu_categories',
															'field' => 'slug',
															'terms' => $term->slug,
													),
											),
											'posts_per_page'    => -1
									);
									$menu = new WP_Query( $arg );
									if ( $menu->have_posts() ) : ?>

										<div class="tab-pane fade <?php if ($index==0){ echo 'active show'; }else{ echo "";}?>" id="<?php echo $term->slug; ?>" role="tabpanel" aria-labelledby="<?php echo $term->slug; ?>-tab">
											<div class="row">
												<?php while ( $menu->have_posts() ) : $menu->the_post(); ?>
													<div class="col-4" >
														<div class="menu-item">
														<?php if( get_field('price')):?>
															<h4><?php the_title();?> .... <?php the_field('price'); ?> USD</h4>
														<?php endif;?>
														<?php if( get_field('description')):?>
															<p class="hp-menu-description"><?php the_field('description'); ?></p>
														<?php endif;?>
														</div>
													</div>
												<?php endwhile; ?>
											</div>
										</div>
									<?php endif; wp_reset_query(); ?>
								<?php } ?>
							<?php endif;?>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="hp-events" id="hp-events" <?php $image = get_field('background_section_events');
		if( !empty( $image ) ): ?>
			style="background:  linear-gradient(0deg, rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.6)), url('<?php the_field('background_section_events'); ?>');
				background-size: cover;
				background-attachment: scroll;
				background-position: center;"
		<?php endif; ?>>
			<div class="container">
				<div class="row">
					<div class="col-12 text-center">
						<?php if(get_field('title_events')):?>
							<h5><?php the_field('title_events'); ?></h5>
						<?php endif;?>
					</div>
					<?php $arg = array(
						'post_type'	        => 'events',
						'order'		        => 'ASC',
						'orderby'	        => 'menu_order',
						'posts_per_page'    => -1
					);
					$events = new WP_Query( $arg );
					if ( $events->have_posts() ) : ?>
						<?php while ( $events->have_posts() ) : $events->the_post(); ?>
							<div class="col-md-6 hp-event-item">
								<div class="item">
									<div class="item-link">
										<a class="event-link" href="<?php the_permalink();?>"><?php the_title();?></a>
									</div>
									<?php the_post_thumbnail('about-block');?>
								</div>
							</div>
						<?php endwhile;
						wp_reset_postdata();
						?>
					<?php endif;?>
					<div class="col-12 text-center">
						<?php if(get_field('phone', 'option')):?>
						<p class="hp-events-info">For private events please call: <?php the_field('phone', 'option');?> or use the contact form.</p>
						<?php endif; ?>
					</div>
				</div>
		</section>
		<section class="hp-gallery" id="hp-gallery">
			<div class="row no-gutters">
				<?php
				$images = get_field('gallery_home');
				if( $images ): ?>
					<div id="hp-gallery-slide" class="slick-slider" >
						<?php foreach( $images as $item ): ?>
							<div class="slick-slide">
								<div class="slider-caption">
									<img class="gallery-item" src="<?php echo esc_url($item['sizes']['large']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
		</section>

		<section class="hp-contact" id="hp-contact">
			<div class="container">
				<div class="row">
					<div class="col-12 text-center">
						<?php if( get_field('title_contact')):?>
							<h2 ><?php the_field('title_contact'); ?></h2>
						<?php endif;?>
						<?php if( get_field('description_contact')):?>
							<p class="hp-description"><strong><?php the_field('description_contact'); ?></strong></p>
						<?php endif;?>
					</div>
				</div>
				<?php if(get_field('shortcode_contact')):?>
					<?php echo do_shortcode(get_field('shortcode_contact'));?>
				<?php endif;?>


				<div class="hp-contact-info">
					<div class="contact-items">
						<?php if(get_field('address', 'option')):?>
							<p class="item"><i class="fa fa-map-marker"></i> <?php the_field('address', 'option');?></p>
						<?php endif;?>
					</div>
					<div class="contact-item">
						<?php if(get_field('phone', 'option')):?>
							<p class="item"><i class="fa fa-phone"></i> <?php the_field('phone', 'option');?></p>
						<?php endif;?>
					</div>
					<div class="contact-item">
						<?php if(get_field('email', 'option')):?>
							<p class="item"><i class="fa fa-envelope"></i> <?php the_field('email', 'option');?></p>
						<?php endif;?>
					</div>
				</div>

			</div>
		</section>

		<section class="hp-map">
			<div id="mapid"></div>
			<?php $location = get_field('local', 'option');
			if($location):?>
				<script>
					var lat = <?php echo $location['lat']?>;
					var lng = <?php echo $location['lng']?>;
				</script>
			<?php endif;?>
		</section>
	</div><!-- #full-width-page-wrapper -->



<?php get_footer();
