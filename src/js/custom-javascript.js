
$(window).on('load', ()=>{
	$('#hp-specialties-slide').slick({
		cssEase: 'ease',
		fade: true,  // Cause trouble if used slidesToShow: more than one
		arrows: false,
		dots: true,
		infinite: true,
		speed: 500,
		autoplay: true,
		autoplaySpeed: 3000,
		slidesToShow: 1,
		slidesToScroll: 1
	});

	$('#hp-gallery-slide').slick({
		cssEase: 'ease',
		fade: false,  // Cause trouble if used slidesToShow: more than one
		arrows: false,
		dots: false,
		infinite: true,
		speed: 500,
		centerMode: false,
		autoplay: true,
		autoplaySpeed: 3000,
		slidesToShow: 4,
		slidesToScroll: 1
	});



	/* var mymap = L.map('mapid').setView([ lat , lng ], 15);
	var popup = L.popup()
		.setLatLng([ lat , lng])
		.setContent("We are here")
		.openOn(mymap);

	L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
		attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
		maxZoom: 20,
		id: 'mapbox/streets-v11',
		accessToken: 'pk.eyJ1Ijoib2xla3NhbmRybHYiLCJhIjoiY2szdmh3NnEzMG1scTNrbGtudnMxeHdwZyJ9.xIIwvK-Epb6VUUZIJGnCaQ'
	}).addTo(mymap); */


	/*Change Placeholder DATE CONTACT FORM 7*/
	$('input[type="date"], input[type="datetime"], input[type="datetime-local"], input[type="month"], input[type="time"], input[type="week"]').each(function() {
		var el = this, type = $(el).attr('type');
		if ($(el).val() == '') $(el).attr('type', 'text');
		$(el).focus(function() {
			$(el).attr('type', type);
			el.click();
		});
		$(el).blur(function() {
			if ($(el).val() == '') $(el).attr('type', 'text');
		});
	});

	var share = $('.hp-contact-info').detach();
	share.appendTo('#contact-info');
});


$(document).ready(function(){
	$('#menu-tab a').click( function (e) {
		e.preventDefault();
		// $('nav-link').removeClass('active');
		$(this).tab('show');
	});
	/*$('#menu-tab a').on('shown.bs.tab', function(event){
		event.target;         // active tab
		event.relatedTarget;  // previous tab

	});*/
});
