<?php
/**
 * Theme basic setup.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

// Set the content width based on the theme's design and stylesheet.
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

add_action( 'after_setup_theme', 'understrap_setup' );

if ( ! function_exists( 'understrap_setup' ) ) {
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function understrap_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on understrap, use a find and replace
		 * to change 'understrap' to the name of your theme in all the template files
		 */
		load_theme_textdomain( 'understrap', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'primary' => __( 'Primary Menu', 'understrap' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		/*
		 * Adding Thumbnail basic support
		 */
		add_theme_support( 'post-thumbnails' );

		/*
		 * Adding support for Widget edit icons in customizer
		 */
		add_theme_support( 'customize-selective-refresh-widgets' );

		/*
		 * Enable support for Post Formats.
		 * See http://codex.wordpress.org/Post_Formats
		 */
		add_theme_support( 'post-formats', array(
			'aside',
			'image',
			'video',
			'quote',
			'link',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'understrap_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Set up the WordPress Theme logo feature.
		add_theme_support( 'custom-logo' );

		// Add support for responsive embedded content.
		add_theme_support( 'responsive-embeds' );

		// Check and setup theme default settings.
		understrap_setup_theme_default_settings();

	}
}


add_filter( 'excerpt_more', 'understrap_custom_excerpt_more' );

if ( ! function_exists( 'understrap_custom_excerpt_more' ) ) {
	/**
	 * Removes the ... from the excerpt read more link
	 *
	 * @param string $more The excerpt.
	 *
	 * @return string
	 */
	function understrap_custom_excerpt_more( $more ) {
		if ( ! is_admin() ) {
			$more = '';
		}
		return $more;
	}
}

add_filter( 'wp_trim_excerpt', 'understrap_all_excerpts_get_more_link' );

if ( ! function_exists( 'understrap_all_excerpts_get_more_link' ) ) {
	/**
	 * Adds a custom read more link to all excerpts, manually or automatically generated
	 *
	 * @param string $post_excerpt Posts's excerpt.
	 *
	 * @return string
	 */
	function understrap_all_excerpts_get_more_link( $post_excerpt ) {
		if ( ! is_admin() ) {
			$post_excerpt = $post_excerpt . ' [...]<p><a class="btn btn-secondary understrap-read-more-link" href="' . esc_url( get_permalink( get_the_ID() ) ) . '">' . __( 'Read More...',
			'understrap' ) . '</a></p>';
		}
		return $post_excerpt;
	}
}


//ACF Pro Options Page

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title'    => 'Theme General Settings',
		'menu_title'    => 'Options Page',
		'menu_slug'     => 'theme-general-settings',
		'capability'    => 'edit_posts',
		'redirect'      => false
	));

}

// SECOND MENU
function wpb_custom_new_menu() {
	register_nav_menus( array(
		'second-menu' => 'Second Menu',
		'mobile-menu' => 'Mobile Menu'
	) );
}
add_action( 'init', 'wpb_custom_new_menu' );

add_image_size( 'about-block', 475, 475, true);


//CUSTOM POST TYPE
// Register Post Type Specialties
function post_type_specialties() {
	$post_type_specialties_labels = array(
		'name'               => _x( 'Specialties', 'post type general name' ),
		'singular_name'      => _x( 'Specialty', 'post type singular name' ),
		'add_new'            => _x( 'Add New', 'specialty' ),
		'add_new_item'       => __( 'Add New' ),
		'edit_item'          => __( 'Edit' ),
		'new_item'           => __( 'New ' ),
		'all_items'          => __( 'All' ),
		'view_item'          => __( 'View' ),
		'search_items'       => __( 'Search for a specialty' ),
		'not_found'          => __( 'No specialties found' ),
		'not_found_in_trash' => __( 'No specialties found in the Trash' ),
		'parent_item_colon'  => '',
		'menu_name'          => 'Specialties'
	);
	$post_type_specialties_args = array(
		'labels'        => $post_type_specialties_labels,
		'description'   => 'Display Specialties',
		'public'        => true,
		'menu_icon'		=> 'dashicons-testimonial',
		'menu_position' => 5,
		'supports'      => array( 'title', 'page-attributes', 'thumbnail', 'editor' ),
		'has_archive'   => true,
		'hierarchical'  => true
	);
	register_post_type( 'Specialty', $post_type_specialties_args );
}
add_action( 'init', 'post_type_specialties' );

// MENU POST TYPE
// Register Post Type menu
function post_type_menus() {
	$post_type_menus_labels = array(
		'name'               => _x( 'Menus', 'post type general name' ),
		'singular_name'      => _x( 'Menu', 'post type singular name' ),
		'add_new'            => _x( 'Add New', 'Dishes' ),
		'add_new_item'       => __( 'Add New' ),
		'edit_item'          => __( 'Edit' ),
		'new_item'           => __( 'New ' ),
		'all_items'          => __( 'All' ),
		'view_item'          => __( 'View' ),
		'search_items'       => __( 'Search for a menu' ),
		'not_found'          => __( 'No specialties found' ),
		'not_found_in_trash' => __( 'No specialties found in the Trash' ),
		'parent_item_colon'  => '',
		'menu_name'          => 'Menu'
	);
	$post_type_menus_args = array(
		'labels'        => $post_type_menus_labels,
		'description'   => 'Display Menus',
		'public'        => true,
		'menu_icon'		=> 'dashicons-carrot',
		'menu_position' => 5,
		'supports'      => array( 'title', 'page-attributes', 'editor' ),
		'has_archive'   => true,
		'hierarchical'  => true
	);
	register_post_type( 'menus', $post_type_menus_args );
}
add_action( 'init', 'post_type_menus' );

function create_menu_taxonomies() {
	$labels = array(
		'name'              => _x( 'Categories', 'taxonomy general name' ),
		'singular_name'     => _x( 'Category', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Categories' ),
		'all_items'         => __( 'All Categories' ),
		'parent_item'       => __( 'Parent Category' ),
		'parent_item_colon' => __( 'Parent Category:' ),
		'edit_item'         => __( 'Edit Category' ),
		'update_item'       => __( 'Update Category' ),
		'add_new_item'      => __( 'Add New Category' ),
		'new_item_name'     => __( 'New Category Name' ),
		'menu_name'         => __( 'Categories' ),
	);

	$args = array(
		'hierarchical'      => true, // Set this to 'false' for non-hierarchical taxonomy (like tags)
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'categories' ),
	);

	register_taxonomy( 'menu_categories', array( 'menus' ), $args );
}
add_action( 'init', 'create_menu_taxonomies');

// Register Post Type Events
function post_type_events() {
	$post_type_events_labels = array(
		'name'               => _x( 'Events', 'post type general name' ),
		'singular_name'      => _x( 'Event', 'post type singular name' ),
		'add_new'            => _x( 'Add New', 'event' ),
		'add_new_item'       => __( 'Add New' ),
		'edit_item'          => __( 'Edit' ),
		'new_item'           => __( 'New ' ),
		'all_items'          => __( 'All' ),
		'view_item'          => __( 'View' ),
		'search_items'       => __( 'Search for a event' ),
		'not_found'          => __( 'No events found' ),
		'not_found_in_trash' => __( 'No events found in the Trash' ),
		'parent_item_colon'  => '',
		'menu_name'          => 'Events'
	);
	$post_type_events_args = array(
		'labels'        => $post_type_events_labels,
		'description'   => 'Display Events',
		'public'        => true,
		'menu_icon'		=> 'dashicons-calendar-alt',
		'menu_position' => 5,
		'supports'      => array( 'title', 'page-attributes', 'thumbnail', 'editor' ),
		'has_archive'   => true,
		'hierarchical'  => true
	);
	register_post_type( 'events', $post_type_events_args );
}
add_action( 'init', 'post_type_events' );
